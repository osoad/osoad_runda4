---------->Alegerea regiuniunilor la inceput<---------
1)voi alege superRegiunea cu cel mai mare raport reward/armies

---------->Miscari de deploy<---------
1)In primele 5 runde cuceresc regiunile care leam ales la inceput
	- voi alege superRegiunea cu cel mai mare raport reward/armies
	- voi alege din acea superRegiune regiunea cu cei mai multi vecini
2)In urmatoarele runde daca am inamic incerc sa ma apar sau sa il atac
	- prioritate au superRegiunile cucerite deja
	- daca nu exista inamic langa superRegiunile deja cucerite voi imparti
	armata la toate regiunile care au inamic
3)Daca nu am adversar incerc sa cuceresc superRegiuni

---------->Miscari de atac<---------

->Stabilirea prioritatii de atac se face in functie de cea facuta la deploy.
->Regiunile inamice sunt parcurse in ordine descrescatoare a prioritatii.
->Se calculeaza suma armatelor ce pot fi puse la dispozitie de regiunile vecine 
	tintei si se testeaza daca atacul merita initiat.
->Armatele se vor trimite in primul rand de la regiunile ce au a doua lor 
	tinta prioritara cu o prioritate mai mica decat restul, iar daca sunt 
	egale de la cea cu cea mai mica armata

---->Mutarea armatelor<----
Mutarea armatelor din regiunile ce au ca vecini numai regiuni ce apartin
(notam cu R o regiune ce respecta acest criteriu).
1) Se bazeaza pe o parcurgere BFS.
	1.1) Graful G = (V, E) unde V = multimea regiunilor ce apartin, 
		E = multimea muchilor dintre regiuni; doua regiuni au muchie daca au o 
		frontiera comuna.
	1.2) Se face o parcurgere BFS pornind din R pentru a se determina cel mai 
		scurt drum de la o regiune R la o regiune de margine (care are cel putin
		 o frontiera comuna cu inamicul sau cu o zona neutra)
	1.3) Daca pe regiunea R se afla N armate, se muta N-1 armate pe prima 
		regiune ce alcatuieste drumul cel mai scurt.

