/**
 * Warlight AI Game Bot
 *
 * Last update: January 29, 2015
 *
 * @author Jim van Eeden
 * @version 1.1
 * @License MIT License (http://opensource.org/Licenses/MIT)
 */

package bot;

/**
 * This is a simple bot that does random (but correct) moves.
 * This class implements the Bot interface and overrides its Move methods.
 * You can implement these methods yourself very easily now,
 * since you can retrieve all information about the match from variable state
 * When the bot decided on the move to make, it returns an ArrayList of Moves. 
 * The bot is started by creating a Parser to which you add
 * a new instance of your bot, and then the parser is started.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import stategies.PlaceArmiesMoves;
import stategies.PlaceArmiesMovesStart;
import stategies.PlaceArmiesMovesWithEnemy;
import stategies.PlaceArmiesMovesWithoutEnemy;

import actions.ConquerSuperRegion;
import actions.ConquerSuperRegions;
import actions.DefendSuperRegion;
import actions.ObtainableSuperRegions;

import map.Region;
import map.SuperRegion;
import move.AttackTransferMove;
import move.PlaceArmiesMove;
import comparators.*;

public class BotStarter implements Bot {
	
	
	LinkedList<Region> visibleMaplst = new LinkedList<>();
	int armiesLeft;
	String myName;
	String enemyName;

	/**
	 * Move Idle Armies
	 */
	/**
	 * 
	 * @param parent - a hashtable with: key - a region R, value - parent of region R;
	 * @param r - the current region; 
	 * @param prec - the precedent region;
	 * @return the region where to move the armies
	 */
	Region getRegion(HashMap<Region, Region> parent, Region r, Region prec) {
		if (parent.get(r) != null) {
			return getRegion(parent, parent.get(r), r);
		}
		return prec;
	}

	Region getBestRegionToMove(BotState state, Region fromRegion) {
		String myName = state.getMyPlayerName();

		HashMap<Region, Region> parent = new HashMap<>();
		HashMap<Region, Integer> viz = new HashMap<>();

		// as a queue
		LinkedList<Region> c = new LinkedList<>();

		parent.put(fromRegion, null);
		viz.put(fromRegion, 0);
		c.addLast(fromRegion);

		Region maxR = null;
		int dist = 9999;

		// BFS algorithm
		while (!c.isEmpty()) {
			Region crtRegion = c.removeFirst();
			LinkedList<Region> allNeighbours = crtRegion.getNeighbors();

			for (Region r : allNeighbours) {
				if (r.ownedByPlayer(myName) && viz.containsKey(r) == false) {

					c.addLast(r);
					viz.put(r, viz.get(crtRegion) + 1);
					parent.put(r, crtRegion);
					// if region r is owned by me, it has at least a border
					// with an enemy and it is the closest one, it has to be saved.
					for (Region neighbour : r.getNeighbors())
						if (!neighbour.ownedByPlayer(myName)) {
							if (viz.get(r) < dist) {
								dist = viz.get(r);
								maxR = r;
							}
						}
				}
			}
		}
		return getRegion(parent, maxR, null);
	}
	/**
	 * End Move Idle Armies 
	 */
	@Override
	/**
	 * A method that returns which region the bot would like to start on, the pickable regions are stored in the BotState.
	 * The bots are asked in turn (ABBAABBAAB) where they would like to start and return a single region each time they are asked.
	 * This method returns one random region from the given pickable regions.
	 */
	public Region getStartingRegion(BotState state, Long timeOut) {
		return state.getPickableStartingRegions().get(0);
	}
	
	@Override
	/**
	 * This method is called for at first part of each round. This example puts two armies on random regions
	 * until he has no more armies left to place.
	 * @return The list of PlaceArmiesMoves for one round
	 */
	
	public ArrayList<PlaceArmiesMove> getPlaceArmiesMoves(BotState state,
			Long timeOut) {
		System.err.println("Round " + state.getRoundNumber());
		System.err.println("PLACE");
		visibleMaplst.clear();
		PlaceArmiesMoves placeArmiesMoves = new PlaceArmiesMoves(state,visibleMaplst);
		return placeArmiesMoves.run();
	}
	@Override
	/**
	 *Method that atacks enemy regions giving prorities according
	 *to their place in visibleMaplst - a list built when deploying armies
	 * @return The list of PlaceArmiesMoves for one round
	 */
	public ArrayList<AttackTransferMove> getAttackTransferMoves(BotState state,
			Long timeOut) {
		
		ArrayList<AttackTransferMove> attackTransferMoves = new ArrayList<AttackTransferMove>();
		String myName = state.getMyPlayerName();
		HashMap<Region, Integer> visibleMap = new HashMap<>();
		int priority = 100;
		System.err.println("ATTACK");
		//calculating the priority of every region and 
		//creating a hashmap with them
		for(Region enemyRegion : visibleMaplst){
			System.err.println(enemyRegion.getId());
			visibleMap.put(enemyRegion, priority--);
				
				
		}
		//iterate through every enemy Region from the highest 
		//priority to the lowest
		for (Region enemyRegion : visibleMaplst) {
			if (!enemyRegion.ownedByPlayer(myName)) {
				LinkedList<Region> friendlyAlliance = new LinkedList<>();

				int atack_availableArmies = 0;
				//finding the regions neighbouring the target enemy region,
				//placing them in the friendlyAlliance list and also
				//calculating the available armies ready for atack
				//from all of them.
				for (Region friendly : enemyRegion.getNeighbors()) {
					if (friendly.ownedByPlayer(myName)) {

						atack_availableArmies += friendly
								.availableArmies(enemyRegion);
						friendlyAlliance.add(friendly);

					}
				}
				int currPriority = visibleMap.get(enemyRegion);
				int requiredArmies = enemyRegion.requiredArmies();
				//test if the atack can be a succes
				if (atack_availableArmies >= requiredArmies) {
					//geting for every region in the alliance 
					//their next prioritary target and if it has chances to
					//succeed
					for (Region friendly : friendlyAlliance) {
						int nextTargetP = 0;
						Region prioritaryEnemy = enemyRegion;
						//looking for enemy neighbours
						for (Region enemy : friendly.getNeighbors()) {
							if (visibleMap.get(enemy) != null) {
								//getting regions priority
								int enemyP = visibleMap.get(enemy);
								if (nextTargetP < enemyP
										&& enemyP != currPriority) {
									nextTargetP = enemyP;
									prioritaryEnemy = enemy;
								}

							}
						}
						
						int atack_availableArmies2 = 0;
						for (Region neighs : enemyRegion.getNeighbors()) {
							if (neighs.ownedByPlayer(myName)) {
								atack_availableArmies2 += neighs
										.availableArmies(prioritaryEnemy);

							}
						}
						
						//testing if the atack can be done and  
						if (atack_availableArmies2 < prioritaryEnemy
								.requiredArmies()) {
							nextTargetP = 0;
						}
						//seting  nextTarget on the neighbour
						friendly.setNextTargetPriority(nextTargetP);
						friendly.setNextTarget(prioritaryEnemy);
					}
					//sorting the friendly regions involved in the atack:
					//1. by the priority of their next target(lowest to highest)
					//2. by the number of armies the region has(lower to highest)
					Collections.sort(friendlyAlliance,
							new NextTargetAndArmyComparator());
					//issuing the atack
					for (Region atacker : friendlyAlliance) {
						int availableArmies = atacker
								.availableArmies(enemyRegion);
						
						if (!(availableArmies == 1 )) {
							//daca urmatoarea tinta a sa are prioritate 0 
							//ataca cu toate armatele
							if (atacker.getNextTargetPriority() == 0
									|| availableArmies < requiredArmies) {
								attackTransferMoves.add(new AttackTransferMove(
										myName, atacker, enemyRegion,
										availableArmies));
								atacker.commitAtack(availableArmies);
								requiredArmies -= availableArmies;
							} else {
								//daca regiunea poate ataca si tinta curenta 
								//si urmatoarea cea mai prioritara a sa
								//ataca cu cate armate este necesar
								if (availableArmies >= requiredArmies
										+ atacker.getNextTarget()
												.requiredArmies()) {
									attackTransferMoves
											.add(new AttackTransferMove(
													myName,
													atacker,
													enemyRegion,
													availableArmies
															- atacker
																	.getNextTarget()
																	.requiredArmies()));
									atacker.commitAtack(availableArmies
											- atacker.getNextTarget()
													.requiredArmies());
									requiredArmies -= availableArmies - atacker.getNextTarget().requiredArmies();
								} else {
									
									attackTransferMoves
											.add(new AttackTransferMove(myName,
													atacker, enemyRegion,
													availableArmies));
									atacker.commitAtack(availableArmies);
									requiredArmies -= availableArmies;

								}
							}
						}

					}
				}
			}
		}
		/**
		 * Move Idle Armies
		 */
		// Searching for my regions which don't have any borders with an enemy
		// region or a neutral region
		// String myName = state.getMyPlayerName();
		for (Region fromRegion : state.getVisibleMap().getRegions()) {
			if (fromRegion.ownedByPlayer(myName)) {
				LinkedList<Region> allNeighbours = fromRegion.getNeighbors();
				boolean ok = true;

				for (Region r : allNeighbours) {
					if (!r.ownedByPlayer(myName)) {
						ok = false;
						break;
					}
				}
				// If fromRegion respects the criteria and it has more than 
				// 1 army I find the best region to move the army using BFS
				if (ok && fromRegion.getArmies() > 1) {
					Region reg = getBestRegionToMove(state, fromRegion);
					AttackTransferMove command = new AttackTransferMove(myName,
							fromRegion, reg, fromRegion.getArmies() - 1);
					attackTransferMoves.add(command);
				}
			}
		}
		return attackTransferMoves;
		/**
		 * End Move Idle Armies
		 */
	}

	public static void main(String[] args) {
		BotParser parser = new BotParser(new BotStarter());
		parser.run();
	}

}
