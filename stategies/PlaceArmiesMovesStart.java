package stategies;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import actions.ConquerSuperRegions;
import bot.BotState;

import map.Region;
import move.PlaceArmiesMove;

public class PlaceArmiesMovesStart {
	BotState state;
	LinkedList<Region> visibleMaplst;
	public PlaceArmiesMovesStart(BotState state,LinkedList<Region> visibleMaplst){
		this.state = state;
		this.visibleMaplst = visibleMaplst;
	}
	public ArrayList<PlaceArmiesMove> run(){
		System.err.println("Start");
		ArrayList<Integer> favouriteSuperRegions = state.getFavouriteSuperRegions();
		int armiesLeft = state.getStartingArmies();
		String myName = state.getMyPlayerName();
		String enemyName = state.getOpponentPlayerName();
		ArrayList<PlaceArmiesMove> placeArmiesMoves = new ArrayList<PlaceArmiesMove>();
		ConquerSuperRegions conquerSuperRegions =
					new ConquerSuperRegions(favouriteSuperRegions,state,armiesLeft,visibleMaplst);
		conquerSuperRegions.run();
		armiesLeft = conquerSuperRegions.getArmiesLeft();
		visibleMaplst = conquerSuperRegions.getAttackEnemyRegions();
		placeArmiesMoves.addAll(conquerSuperRegions.getPlaceArmiesMoves());
		/*
		if(armiesLeft > 0){
			System.out.println("armiesLeft start " + armiesLeft);
			ArrayList<Region> myFrontlineRegions = new ArrayList<Region>();
			for (Region enemyRegion : state.getVisibleMap().getRegions()) {
				if (enemyRegion.ownedByPlayer(enemyName)) {
					if(!visibleMaplst.contains(enemyRegion)){
						visibleMaplst.add(enemyRegion);
					}
					for (Region enemyRegionNeighbor : enemyRegion.getNeighbors()) {
						if (enemyRegionNeighbor.ownedByPlayer(myName) ) {
							if (!myFrontlineRegions.contains(enemyRegionNeighbor)) {
								myFrontlineRegions.add(enemyRegionNeighbor);
							}
						}
					}
				}
			}
			if(myFrontlineRegions.size() != 0){
				placeArmiesMoves.addAll(getPlaceArmiesMovesEnemy(state,myFrontlineRegions));
			}
			else{
				placeArmiesMoves.addAll(getPlaceArmiesMovesWithOutEnemy(state));
			}
		}*/
		return placeArmiesMoves;
	}
	
}
