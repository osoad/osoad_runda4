package stategies;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

import utils.FrontlineRegions;

import comparators.RegionArmyComparator;

import map.Region;
import move.PlaceArmiesMove;
import actions.ConquerSuperRegions;
import actions.DamageEnemy;
import actions.ObtainableSuperRegions;
import actions.PlaceArmyEqualy2Regions;
import bot.BotState;

public class PlaceArmiesMovesWithoutEnemy {
	BotState state;
	LinkedList<Region> visibleMaplst;
	ArrayList<PlaceArmiesMove> placeArmiesMoves;
	int armiesLeft;
	String myName;
	String enemyName;
	public PlaceArmiesMovesWithoutEnemy(BotState state,LinkedList<Region> visibleMaplst){
		this.state = state;
		this.visibleMaplst = visibleMaplst;
		this.placeArmiesMoves = new ArrayList<PlaceArmiesMove>();
		this.armiesLeft = state.getStartingArmies();
		this.myName = state.getMyPlayerName();
		this.enemyName = state.getOpponentPlayerName();
	}
	public ArrayList<PlaceArmiesMove> run(){
		System.err.println("Without Enemy");
		getConquerSuperRegions();
		getNextSuperRegion();
		getPlaceArmyEqualy2Regions();
		return placeArmiesMoves;
	}
	private void getNextSuperRegion() {
		
	}
	private void getConquerSuperRegions() {
		ObtainableSuperRegions obtainableSuperRegions = new ObtainableSuperRegions(state);
		obtainableSuperRegions.run();
		ArrayList<Integer> favouriteSuperRegions = obtainableSuperRegions.SuperRegionsIds();
		ConquerSuperRegions conquerSuperRegions =
				new ConquerSuperRegions(favouriteSuperRegions,state,armiesLeft,visibleMaplst);
		conquerSuperRegions.run();
		armiesLeft = conquerSuperRegions.getArmiesLeft();
		visibleMaplst = conquerSuperRegions.getAttackEnemyRegions();
		placeArmiesMoves.addAll(conquerSuperRegions.getPlaceArmiesMoves());
	}
	private void getPlaceArmyEqualy2Regions() {
		PlaceArmyEqualy2Regions placeArmyEqualy2Regions =
				new PlaceArmyEqualy2Regions(getFrontlineRegions(),state,armiesLeft,visibleMaplst);
		placeArmyEqualy2Regions.run();
		armiesLeft = placeArmyEqualy2Regions.getArmiesLeft();
		visibleMaplst = placeArmyEqualy2Regions.getAttackEnemyRegions();
		placeArmiesMoves.addAll(placeArmyEqualy2Regions.getPlaceArmiesMoves());
	
	}
	private ArrayList<Region> getFrontlineRegions(){
		FrontlineRegions frontlineRegions =
				new FrontlineRegions(state,visibleMaplst);
		return frontlineRegions.run();
	}
	
}
