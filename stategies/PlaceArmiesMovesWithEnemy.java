package stategies;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import utils.FrontlineRegions;

import comparators.FrontlineRegionsComparator;
import comparators.SuperRegionRewardComparator;

import map.Region;
import map.SuperRegion;
import move.PlaceArmiesMove;
import actions.ConquerSuperRegions;
import actions.DamageEnemy;
import actions.PlaceArmies2Regions;
import actions.PlaceArmies2RegionsConquer;
import bot.BotState;

public class PlaceArmiesMovesWithEnemy {
	BotState state;
	LinkedList<Region> visibleMaplst;
	int armiesLeft;
	ArrayList<PlaceArmiesMove> placeArmiesMoves;
	String myName;
	String enemyName;
	public PlaceArmiesMovesWithEnemy(BotState state,LinkedList<Region> visibleMaplst){
		this.state = state;
		this.visibleMaplst = visibleMaplst;
		this.armiesLeft = state.getStartingArmies();
		this.placeArmiesMoves = new ArrayList<PlaceArmiesMove>();
		this.myName = state.getMyPlayerName();
		this.enemyName = state.getOpponentPlayerName();
	}
	
	public ArrayList<PlaceArmiesMove> run(){
		System.err.println("With Enemy");
		String myName = state.getMyPlayerName();
		ArrayList<Region> myFrontlineRegions = getFrontlineRegions();
		Collections.sort(myFrontlineRegions, new FrontlineRegionsComparator(state.getMyPlayerName(),state.getOpponentPlayerName()));
		ArrayList<Region> importantRegions = new ArrayList<Region>();
		ArrayList<Region> frontlineRegions = new ArrayList<Region>();
		for(Region region : myFrontlineRegions){
			if(myName.equals( region.getSuperRegion().ownedByPlayer())){
				importantRegions.add(region);
			}
			else{
				frontlineRegions.add(region);
			}
		}
		Collections.sort(importantRegions,new SuperRegionRewardComparator());
		Collections.sort(frontlineRegions,new SuperRegionRewardComparator());
		if(importantRegions.size() > 0){
			getPlaceArmies2Regions(importantRegions);
			if(armiesLeft > 0){
				//getDamageEnemy();
				if(armiesLeft > 0){
					getPlaceArmies2RegionsConquer(importantRegions);

					noImportantRegions(frontlineRegions);
				}
			}
		} 
		else {
			//getDamageEnemy();
			if(armiesLeft > 0){
				noImportantRegions(frontlineRegions);
			}
		}
		return placeArmiesMoves;
	
	}
	

	private void noImportantRegions(ArrayList<Region> regions){
		if(armiesLeft > 0){
			getPlaceArmies2Regions(regions);
			if(armiesLeft > 0){	
				getPlaceArmies2RegionsConquer(regions);
				if(armiesLeft > 0){
					PlaceArmiesMovesWithoutEnemy placeArmiesMovesWithoutEnemy = 
							new PlaceArmiesMovesWithoutEnemy(state,visibleMaplst);
					placeArmiesMoves.addAll(placeArmiesMovesWithoutEnemy.run());
				}
			}
		}
	}
	private void getDamageEnemy() {
		DamageEnemy damageEnemy = new DamageEnemy(state,armiesLeft,visibleMaplst);
		damageEnemy.run();
		armiesLeft = damageEnemy.getArmiesLeft();
		visibleMaplst = damageEnemy.getAttackEnemyRegions();
		placeArmiesMoves.addAll(damageEnemy.getPlaceArmiesMoves());
	}
	private void getPlaceArmies2Regions(ArrayList<Region> regions ){
		PlaceArmies2Regions placeArmies2Regions =
				new PlaceArmies2Regions(regions,state,armiesLeft,visibleMaplst);
		placeArmies2Regions.run();
		armiesLeft = placeArmies2Regions.getArmiesLeft();
		visibleMaplst = placeArmies2Regions.getAttackEnemyRegions();
		placeArmiesMoves.addAll(placeArmies2Regions.getPlaceArmiesMoves());
	}
	private void getPlaceArmies2RegionsConquer(ArrayList<Region> regions){
		PlaceArmies2RegionsConquer placeArmies2RegionsConquer =
				new PlaceArmies2RegionsConquer(regions,state,armiesLeft,visibleMaplst);
		placeArmies2RegionsConquer.run();
		armiesLeft = placeArmies2RegionsConquer.getArmiesLeft();
		visibleMaplst = placeArmies2RegionsConquer.getAttackEnemyRegions();
		placeArmiesMoves.addAll(placeArmies2RegionsConquer.getPlaceArmiesMoves());
		
	}
	private ArrayList<Region> getFrontlineRegions(){
		FrontlineRegions frontlineRegions =
				new FrontlineRegions(state,visibleMaplst);
		return frontlineRegions.run();
	}
	
}
