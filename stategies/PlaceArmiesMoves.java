package stategies;

import java.util.ArrayList;
import java.util.LinkedList;

import map.Region;
import move.PlaceArmiesMove;
import actions.ConquerSuperRegions;
import bot.BotState;

public class PlaceArmiesMoves {
	BotState state;
	LinkedList<Region> visibleMaplst;
	public PlaceArmiesMoves(BotState state,LinkedList<Region> visibleMaplst){
		this.state = state;
		this.visibleMaplst = visibleMaplst;
	}
	public ArrayList<PlaceArmiesMove> run(){
		if(state.getRoundNumber() < 6){
			return placeArmiesMovesStart(state);
		}
		int armiesLeft = state.getStartingArmies();
		
		ArrayList<Region> myFrontlineRegions = getFrontlineRegions();
		if(myFrontlineRegions.size() != 0)
			return PlaceArmiesMovesEnemy(state,myFrontlineRegions);
		else{
			return PlaceArmiesMovesNoEnemy(state);
		}
	}
	private ArrayList<Region> getFrontlineRegions(){
		String myName = state.getMyPlayerName();
		String enemyName = state.getOpponentPlayerName();
		ArrayList<Region> myFrontlineRegions = new ArrayList<Region>();
		for (Region enemyRegion : state.getVisibleMap().getRegions()) {
			if (enemyRegion.ownedByPlayer(enemyName)) {
				if(!visibleMaplst.contains(enemyRegion)){
					visibleMaplst.add(enemyRegion);
				}
				
				for (Region enemyRegionNeighbor : enemyRegion.getNeighbors()) {
					if (enemyRegionNeighbor.ownedByPlayer(myName) ) {
						if (!myFrontlineRegions.contains(enemyRegionNeighbor)) {
							myFrontlineRegions.add(enemyRegionNeighbor);
						}
					}
				}
			}
		}
		return myFrontlineRegions;
	}
	private ArrayList<PlaceArmiesMove> placeArmiesMovesStart(BotState state){
		PlaceArmiesMovesStart placeArmiesMovesStart = 
				new PlaceArmiesMovesStart(state,visibleMaplst);
		return placeArmiesMovesStart.run();
	}
	private ArrayList<PlaceArmiesMove> PlaceArmiesMovesNoEnemy(BotState state){
		PlaceArmiesMovesWithoutEnemy placeArmiesMovesWithoutEnemy = 
				new PlaceArmiesMovesWithoutEnemy(state,visibleMaplst);
		return placeArmiesMovesWithoutEnemy.run();
	}
	private ArrayList<PlaceArmiesMove> PlaceArmiesMovesEnemy(BotState state,ArrayList<Region> myFrontlineRegions){
		PlaceArmiesMovesWithEnemy placeArmiesMovesWithEnemy = 
				new PlaceArmiesMovesWithEnemy(state,visibleMaplst);
		return placeArmiesMovesWithEnemy.run();
	}
}
