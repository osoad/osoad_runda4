package comparators;

import java.util.Comparator;

import map.Region;

public class FrontlineRegionsComparator implements Comparator<Region>{
	String myName;
	String opponentName;
	public FrontlineRegionsComparator(String myName,String opponentName){
		super();
		this.myName = myName;
		this.opponentName = opponentName;
	}
	@Override
	public int compare(Region region1, Region region2) {
		if(myName.equals(region1.getSuperRegion().ownedByPlayer()) && 
				myName.equals(region1.getSuperRegion().ownedByPlayer())){
			if(region1.getOpponentArmies(opponentName) < region2.getOpponentArmies(opponentName)){
				return 1;
			} else {
				return -1;
			}
		}
		else{
			if(myName.equals(region1.getSuperRegion().ownedByPlayer()) || 
					myName.equals(region1.getSuperRegion().ownedByPlayer())){
				if(myName.equals(region1.getSuperRegion().ownedByPlayer())){
					return 1;
				} else {
					return -1;
				}
			}
			else{
				if(region1.getOpponentArmies(opponentName) < region2.getOpponentArmies(opponentName)){
					return 1;
				} else {
					return -1;
				}
			}
		}
	}
}