package comparators;

import java.util.Comparator;

import map.Region;

public class StartingRegionsComparator implements Comparator<Region>{
	public int compare(Region r1, Region r2) {
		if(r1.getPickScore() < r2.getPickScore()){
			return 1;
		} else {
			return -1;
		}
	}
}
