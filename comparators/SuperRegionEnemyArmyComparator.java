package comparators;

import java.util.Comparator;

import map.Map;
import map.Region;
import map.SuperRegion;

public class SuperRegionEnemyArmyComparator implements Comparator<SuperRegion>{
	String myName;
	Map fogMap;
	public SuperRegionEnemyArmyComparator(String myName,Map fogMap){
		super();
		this.myName = myName;
		this.fogMap = fogMap;
	}
	@Override
	public int compare(SuperRegion superRegion1, SuperRegion superRegion2) {
		SuperRegion superRegion1Fog = fogMap.getSuperRegion(superRegion1.getId());
		SuperRegion superRegion2Fog = fogMap.getSuperRegion(superRegion2.getId());
		float armiesReward1 = superRegion1.getArmiesReward();
		float armiesReward2 = superRegion2.getArmiesReward();
		if(superRegion1Fog.getEnemyArmy(myName) / armiesReward1 > 
			superRegion2Fog.getEnemyArmy(myName) / armiesReward2){
			return 1;
		} else {
			return -1;
		}
	}

}
