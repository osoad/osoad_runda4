package comparators;

import java.util.Comparator;

import map.Map;
import map.Region;
import map.SuperRegion;

public class NumberOfEnemyRsInSRComparator implements Comparator<SuperRegion>{
	String myName;
	public NumberOfEnemyRsInSRComparator(String myName){
		super();
		this.myName = myName;
	}
	private int value(SuperRegion superRegion){
		return superRegion.getSubRegions().size() -
				superRegion.getSubRegionsOwned(myName).size();
	}
	@Override
	public int compare(SuperRegion superRegion1, SuperRegion superRegion2) {
		
		if(value(superRegion1) > value(superRegion2)){
			return 1;
		} else {
			if(value(superRegion1) == value(superRegion2)){
				if(superRegion1.getArmiesReward() < superRegion2.getArmiesReward()){
					return 1;
				} else {
					return -1;
				}
			} else {
				return -1;
			}
		}
			
	}

}
