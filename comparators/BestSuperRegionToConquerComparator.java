package comparators;

import java.util.Comparator;

import javax.swing.plaf.metal.MetalIconFactory.FolderIcon16;

import map.Region;
import map.SuperRegion;

public class BestSuperRegionToConquerComparator implements Comparator<SuperRegion> {
	private String myName;
	public BestSuperRegionToConquerComparator(String name) {
		myName = name;
	}
	public int getArmies(SuperRegion superRegion){
		int armies = 0;
		for(Region region : superRegion.getSubRegions()){
			if(region.ownedByPlayer(myName)){	
				armies -= region.getArmies();
			}
			else{
				armies += region.getArmies();
			}
		}
		return armies;
	}
	@Override
	public int compare(SuperRegion superRegion1, SuperRegion superRegion2) {
		int armies1 = getArmies(superRegion1);
		int armies2 = getArmies(superRegion1);
		int reward1 = superRegion1.getArmiesReward();
		int reward2 = superRegion2.getArmiesReward();
		if( (float) reward1/armies1 < (float) reward2/armies2 ){
			return 1;
		} else {
			return -1;
		}
	}

}
