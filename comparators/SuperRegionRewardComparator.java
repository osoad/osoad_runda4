package comparators;

import java.util.Comparator;

import map.Region;
import map.SuperRegion;

public class SuperRegionRewardComparator implements Comparator<Region>{


	@Override
	public int compare(Region region1, Region region2) {
		if(region1.getSuperRegion().getArmiesReward() < region2.getSuperRegion().getArmiesReward()){
			return 1;
		} else {
			return -1;
		}
	}
	
}
