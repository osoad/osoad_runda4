package comparators;

import java.util.Comparator;

import map.Region;

public class RegionArmyComparator implements Comparator<Region>{
	@Override
	public int compare(Region region1, Region region2) {
		if(region1.getArmies() < region2.getArmies()){
			return 1;
		} else {
			return -1;
		}
	}
}
