package comparators;

import java.util.Comparator;

import map.Region;
/**
 * favours the region with the least prioritary next target
 *and with the smallest army in this order
 */
public class NextTargetAndArmyComparator implements Comparator<Region>{

	@Override
	public int compare(Region o1, Region o2) {
		if(o1.getNextTargetPriority() == o2.getNextTargetPriority()){
			return o2.getArmies() - o1.getArmies();
		}
			return o2.getNextTargetPriority() - o1.getNextTargetPriority();
	}

}
