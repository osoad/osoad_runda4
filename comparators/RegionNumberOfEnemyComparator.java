package comparators;

import java.util.Comparator;

import map.Region;

public class RegionNumberOfEnemyComparator implements Comparator<Region>{
	String myName;
	public RegionNumberOfEnemyComparator(String myName){
		super();
		this.myName = myName;
	}
	private int value(Region region){
		return region.getEnemyNeighborsInsideSuperRegion(myName).size();
	}
	@Override
	public int compare(Region region1, Region region2) {
		if(value(region1) < value(region2)){
			return 1;
		} else {
			return -1;
		}
	}

}
