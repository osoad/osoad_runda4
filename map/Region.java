/**
 * Warlight AI Game Bot
 *
 * Last update: January 29, 2015
 *
 * @author Jim van Eeden
 * @version 1.1
 * @License MIT License (http://opensource.org/Licenses/MIT)
 */

package map;

import java.util.LinkedList;


public class Region {
	
	private int id;
	private LinkedList<Region> neighbors;
	private LinkedList<Region> NeighborsInsideSuperRegion;
	private SuperRegion superRegion;
	private int armies;
	private String playerName;
	private int nextTargetPriority;
	private Region nextTarget;
	private int tempArmies = 0;
	private int usedArmies;
	//starting pick
	private float pickScore;
	public Region(int id, SuperRegion superRegion)
	{
		this.id = id;
		this.superRegion = superRegion;
		this.neighbors = new LinkedList<Region>();
		this.playerName = "unknown";
		this.armies = 0;
		
		superRegion.addSubRegion(this);
		
		this.NeighborsInsideSuperRegion = new LinkedList<Region>();
	}
	
	public Region(int id, SuperRegion superRegion, String playerName, int armies)
	{
		this.id = id;
		this.superRegion = superRegion;
		this.neighbors = new LinkedList<Region>();
		this.playerName = playerName;
		this.armies = armies;
		
		superRegion.addSubRegion(this);
		
		this.NeighborsInsideSuperRegion = new LinkedList<Region>();
	}
	
	public void addNeighbor(Region neighbor)
	{
		if(!neighbors.contains(neighbor))
		{
			if( neighbor.getSuperRegion().getId() == superRegion.getId() ){
				NeighborsInsideSuperRegion.add(neighbor);
			}
			neighbors.add(neighbor);
			neighbor.addNeighbor(this);
		}
	}
	
	/**
	 * @param region a Region object
	 * @return True if this Region is a neighbor of given Region, false otherwise
	 */
	public boolean isNeighbor(Region region)
	{
		if(neighbors.contains(region))
			return true;
		return false;
	}

	/**
	 * @param playerName A string with a player's name
	 * @return True if this region is owned by given playerName, false otherwise
	 */
	public boolean ownedByPlayer(String playerName)
	{
		if(playerName.equals(this.playerName))
			return true;
		return false;
	}
	
	/**
	 * @param armies Sets the number of armies that are on this Region
	 */
	public void setArmies(int armies) {
		this.armies = armies;
	}
	
	/**
	 * @param playerName Sets the Name of the player that this Region belongs to
	 */
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	/**
	 * @return The id of this Region
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @return A list of this Region's neighboring Regions
	 */
	public LinkedList<Region> getNeighbors() {
		return neighbors;
	}
	
	/**
	 * @return The SuperRegion this Region is part of
	 */
	public SuperRegion getSuperRegion() {
		return superRegion;
	}
	
	/**
	 * @return The number of armies on this region
	 */
	public int getArmies() {
		return armies;
	}
	
	/**
	 * @return A string with the name of the player that owns this region
	 */
	public String getPlayerName() {
			return playerName;
	}
	/** 
	 * |**
	 * @param R regiunea pe care vrei sa o ataci(va fi exclusa din calcul)
	 * @return numarul de armate pe care le poate oferi regiunea
	 * (ca sa nu ramana neaparata )
	 */
	public int availableArmies(Region R){
		int available_armies = armies + tempArmies - 1;
		LinkedList<Region> possible_enemies = getNeighbors();
		for(Region enemy : possible_enemies){
			if(R.playerName.equals("neutral")) { break;}
			if(enemy.ownedByPlayer(R.playerName) && enemy.getId() != R.getId()){
				//susceptibil la acordari :)
				available_armies -= enemy.armies;
			}
		}
		return available_armies == 1 ? 0 : available_armies;
		
	}
	public int getNextTargetPriority(){
		return nextTargetPriority;
	}
	public void setNextTargetPriority(int priority){
		nextTargetPriority = priority;
	}

	public void setNextTarget(Region prioritaryEnemy) {
		 nextTarget = prioritaryEnemy;
		
	}

	public Region getNextTarget() {
		return nextTarget;
	}

	public int requiredArmies() {
		//trebuie acordat :)
		return Math.round(3/2.0f*armies);
	}
	public void addTempArmies(int i){
		tempArmies += i;
	}
	public void setTempArmies(int i) {
		tempArmies = i;
		
	}
	public int getTempArmies(){
		return tempArmies;
	}
	
	// stating pick
	public void setPickScore(float pickScore){
		this.pickScore = pickScore;
	}
	public float getPickScore(){
		return pickScore;
	}

	public void commitAtack(int i) {
		tempArmies -= i;
		if(tempArmies < 0){
			armies += tempArmies;
			tempArmies = 0;
		}
		
	}
	
	//deploy functions
	public int getEnemyNeighborsInsideSuperRegionArmy(String enemyName){
		int army = 0;
		for(Region region : NeighborsInsideSuperRegion){
			if(region.getPlayerName().equals(enemyName)){
				army += region.getArmies();
			}
		}
		if(army == 0)
			return Integer.MAX_VALUE;
		return army;
	}
	public int getNumberOfEnemyRegionsInsideSuperRegion(String myName){
		int enemy = 0;
		for(Region region : NeighborsInsideSuperRegion){
			if(region.getPlayerName() == null || !region.getPlayerName().equals(myName)){
				enemy += 1;
			}
		}
		return enemy;
	}
	public LinkedList<Region> getEnemyNeighborsInsideSuperRegion(String myName){
		LinkedList<Region> enemy = new LinkedList<Region>();
		for(Region region : NeighborsInsideSuperRegion){
			if(region.isNeighbor(this)){
				if(region.getPlayerName() == null || !region.getPlayerName().equals(myName)){
					enemy.add(region);
				}
			}
		}
		return enemy;
	}
	public int getOpponentArmies(String opponentName){
		int armies = 0;
		for(Region region : neighbors){
			if(region.ownedByPlayer(opponentName)){
				armies += region.getArmies();
			}
		}
		return armies;
	}
	
}
