/**
 * Warlight AI Game Bot
 *
 * Last update: January 29, 2015
 *
 * @author Jim van Eeden
 * @version 1.1
 * @License MIT License (http://opensource.org/Licenses/MIT)
 */

package map;
import java.util.LinkedList;

public class SuperRegion {
	
	private int id;
	private int armiesReward;
	private LinkedList<Region> subRegions;
	
	
	//pick starting regions
	private LinkedList<Region> subRegionsNeighbors;
	private LinkedList<SuperRegion> superRegionsNeighbors;
	

	
	public SuperRegion(int id, int armiesReward)
	{
		this.id = id;
		this.armiesReward = armiesReward;
		subRegions = new LinkedList<Region>();
		
		subRegionsNeighbors = new LinkedList<Region>();
		superRegionsNeighbors = new LinkedList<SuperRegion>();
		
		
	}
	
	public void addSubRegion(Region subRegion)
	{
		if(!subRegions.contains(subRegion))
			subRegions.add(subRegion);
	}
	
	/**
	 * @return A string with the name of the player that fully owns this SuperRegion
	 */
	public String ownedByPlayer()
	{
		//System.out.println("superRegion "+ id);
		String playerName = subRegions.getFirst().getPlayerName();
		//System.out.println("size " + subRegions.size());
		for(Region region : subRegions)
		{
			//System.out.println(region.getId() + " " + region.getPlayerName());
			if (!playerName.equals(region.getPlayerName()))
				return null;
		}
		
		return playerName;
	}
	
	/**
	 * @return The id of this SuperRegion
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @return The number of armies a Player is rewarded when he fully owns this SuperRegion
	 */
	public int getArmiesReward() {
		return armiesReward;
	}
	
	/**
	 * @return A list with the Regions that are part of this SuperRegion
	 */
	public LinkedList<Region> getSubRegions() {
		return subRegions;
	}
	
	public void addSubRegionNeighbor(Region subRegion)
	{
		if(!subRegionsNeighbors.contains(subRegion))
			subRegionsNeighbors.add(subRegion);
	}
	
	public void addSuperRegionNeighbor(SuperRegion superRegion)
	{
		if(!superRegionsNeighbors.contains(superRegion))
			superRegionsNeighbors.add(superRegion);
	}
	
	public LinkedList<Region> getSubRegionNeighbor()
	{
		return subRegionsNeighbors;
	}
	
	public LinkedList<SuperRegion> getSuperRegionNeighbor()
	{
		for( Region region : subRegions ){
			for( Region neiRegion: region.getNeighbors()){
				SuperRegion superRegion = neiRegion.getSuperRegion();
				if(!superRegionsNeighbors.contains(superRegion)
						&& superRegion != this)
					superRegionsNeighbors.add(superRegion);
			
			}
		}
		return superRegionsNeighbors;
	}
	
	
	public LinkedList<Region> getSubRegionsOwned(String myName){
		LinkedList<Region> subRegionsOwned = new LinkedList<Region>();
		for(Region region : subRegions){
			if(region.ownedByPlayer(myName)){
				subRegionsOwned.add(region);
			}
		}
		return subRegionsOwned;
	}
	
	
	public int getEnemyArmy(String myName){
		int army = 0;
		//System.err.println("subregions number " + subRegions.size());
		for(Region region : subRegions){
			//System.err.println("region id " + region.getId() + " armies " + region.getArmies());
			if(!region.ownedByPlayer(myName)){
				army += region.getArmies();
			}
		}
		if(army == 0){
			return Integer.MAX_VALUE;
		}
		return army;
	}

	public LinkedList<Region> getEnemyRegions(String myName) {
		LinkedList<Region> enemyRegions = new LinkedList<Region>();
		for(Region region : subRegions){
			System.err.println("region id " + region.getId() + " armies " + region.getArmies());
			if(!region.ownedByPlayer(myName)){
				enemyRegions.add(region);
			}
		}
		return enemyRegions;
	}
	
}
