package actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import comparators.EnemyRegionComparator;

import map.Region;
import map.SuperRegion;
import move.PlaceArmiesMove;

public class ConquerSuperRegion {
	SuperRegion superRegion;
	int armiesLeft;
	LinkedList<Region> visibleMaplst;
	ArrayList<PlaceArmiesMove> placeArmiesMoves;
	String myName;
	String enemyName;
	public ConquerSuperRegion(
			SuperRegion superRegion,
			int armiesLeft,
			LinkedList<Region> visibleMaplst,
			String myName,
			String enemyName){
		this.superRegion = superRegion;
		this.armiesLeft = armiesLeft;
		this.visibleMaplst = visibleMaplst;
		this.placeArmiesMoves = new ArrayList<PlaceArmiesMove>();
		this.myName = myName;
		this.enemyName = enemyName;
	}
	public void run(){
		ArrayList<Region> enemyRegions = new ArrayList<Region>();
		for(Region region : superRegion.getSubRegions()){
			if(!region.ownedByPlayer(myName)){
				enemyRegions.add(region);
			}

		}
		Collections.sort(enemyRegions,new EnemyRegionComparator(enemyName));
		System.err.println("superRegion id "+ superRegion.getId() + " enemys " + enemyRegions.size());
		ArrayList<Region> usedRegions = new ArrayList<Region>();
		for(Region enemyRegion : enemyRegions){
			System.err.println("enemy id " + enemyRegion.getId() );
			if(!visibleMaplst.contains(enemyRegion)){
				visibleMaplst.add(enemyRegion);	
			}
			int armies = 0;
			for(Region region : enemyRegion.getNeighbors()){
				if(!usedRegions.contains(region) && region.ownedByPlayer(myName)){
					usedRegions.add(region);
					armies += region.getArmies() - 1;
					//System.err.println("armies " + armies + " id " + region.getId());
				}
			}
			int nrOfArmies = enemyRegion.requiredArmies() - armies + 1; 
			//System.err.println("nr " + nrOfArmies + " required " + enemyRegion.requiredArmies() + " " + armies);
			if(nrOfArmies > 0){
				for(Region region : enemyRegion.getNeighbors()){
					if(region.ownedByPlayer(myName)){
						if(nrOfArmies < armiesLeft ){
							System.err.println("my id " + region.getId() + " armies " + nrOfArmies);
							armiesLeft -= nrOfArmies; 
							region.addTempArmies(nrOfArmies);
							placeArmiesMoves.add( new PlaceArmiesMove(myName, region, nrOfArmies) );
							break;
						}
						else{
							System.err.println("my id " + region.getId() + " armies " + armiesLeft);

							region.addTempArmies(armiesLeft);
							placeArmiesMoves.add( new PlaceArmiesMove(myName, region, armiesLeft) );
							armiesLeft = 0;
							return;
						}
					}

				}
			}
		}
	}
	public int getArmiesLeft(){
		return armiesLeft;
	}
	public LinkedList<Region> getAttackEnemyRegions(){
		return visibleMaplst;
	}
	public ArrayList<PlaceArmiesMove> getPlaceArmiesMoves(){
		return placeArmiesMoves;
	}
	
}
