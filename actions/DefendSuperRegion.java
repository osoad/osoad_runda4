package actions;

import java.util.ArrayList;
import java.util.LinkedList;

import map.Region;
import map.SuperRegion;
import move.PlaceArmiesMove;

public class DefendSuperRegion {
	SuperRegion superRegion;
	int armiesLeft;
	LinkedList<Region> visibleMaplst;
	ArrayList<PlaceArmiesMove> placeArmiesMoves;
	String myName;
	String enemyName;
	public DefendSuperRegion(
			SuperRegion superRegion,
			int armiesLeft,
			LinkedList<Region> visibleMaplst,
			String myName,
			String enemyName){
		this.superRegion = superRegion;
		this.armiesLeft = armiesLeft;
		this.visibleMaplst = visibleMaplst;
		this.placeArmiesMoves = new ArrayList<PlaceArmiesMove>();
		this.myName = myName;
		this.enemyName = enemyName;
	}
	public void run(){
		ArrayList<Region> regions = new ArrayList<Region>();
		for(Region region : superRegion.getSubRegions()){
			if(region.ownedByPlayer(myName)){
				regions.add(region);
			}
		}
		System.err.println("superRegion id "+ superRegion.getId());
		ArrayList<Region> usedRegions = new ArrayList<Region>();
		int armies = 0;
		for(Region myRegion : regions){
			System.err.println("my id " + myRegion.getId());
			for(Region region : myRegion.getNeighbors()){
				if(!usedRegions.contains(region) && region.ownedByPlayer(enemyName)){
				//if(region.ownedByPlayer(enemyName)){
					System.err.println("enemy " + region.getId());
					usedRegions.add(region);
					armies += region.requiredArmies();
					if(!visibleMaplst.contains(region)){
						visibleMaplst.add(region);
					}
				}
			}
			int nrOfArmies = armies - myRegion.getArmies();
			if(nrOfArmies > 0){
				if(nrOfArmies < armiesLeft ){
					System.err.println("my id " + myRegion.getId() + " armies " + nrOfArmies);
					armiesLeft -= nrOfArmies; 
					myRegion.addTempArmies(nrOfArmies);
					placeArmiesMoves.add( new PlaceArmiesMove(myName, myRegion, nrOfArmies) );
					break;
				}
				else{
					System.err.println("my id " + myRegion.getId() + " armies " + armiesLeft);
					myRegion.addTempArmies(armiesLeft);
					placeArmiesMoves.add( new PlaceArmiesMove(myName, myRegion, armiesLeft) );
					armiesLeft = 0;
					return;
				}
			}
		}
	}
	public int getArmiesLeft(){
		return armiesLeft;
	}
	public LinkedList<Region> getAttackEnemyRegions(){
		return visibleMaplst;
	}
	public ArrayList<PlaceArmiesMove> getPlaceArmiesMoves(){
		return placeArmiesMoves;
	}
}
