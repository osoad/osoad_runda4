package actions;

import java.util.ArrayList;
import java.util.Collections;

import map.SuperRegion;

import comparators.BestSuperRegionToConquerComparator;

import bot.BotState;

public class ObtainableSuperRegions {
	BotState state;
	ArrayList<Integer> superRegionsIds;
	public ObtainableSuperRegions(BotState state){
		this.state = state;
		superRegionsIds = new ArrayList<Integer>();
	}
	public void run(){
		String myName = state.getMyPlayerName();
		ArrayList<Integer> superRegionsIds =  new ArrayList<Integer>();
		ArrayList<SuperRegion> superRegions =  new ArrayList<SuperRegion>();
		for(SuperRegion superRegion : state.getVisibleMap().getSuperRegions()){
			if(myName.equals(superRegion.ownedByPlayer())){
				superRegions.add(superRegion);
			}
		}
		Collections.sort(superRegions,new BestSuperRegionToConquerComparator(myName));
		for(SuperRegion superRegion : superRegions){
			superRegionsIds.add(superRegion.getId());
		}
	}
	public ArrayList<Integer> SuperRegionsIds(){
		return superRegionsIds;
	}
}
