package actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

import bot.BotState;

import map.Region;
import move.PlaceArmiesMove;

import comparators.RegionArmyComparator;

public class PlaceArmyEqualy2Regions {
	ArrayList<Region> myFrontlineRegions;
	BotState state;
	int armiesLeft;
	LinkedList<Region> visibleMaplst;
	ArrayList<PlaceArmiesMove> placeArmiesMoves;
	String myName;
	String enemyName;
	public PlaceArmyEqualy2Regions(
			ArrayList<Region> myFrontlineRegions,
			BotState state,
			int armiesLeft,
			LinkedList<Region> visibleMaplst){
		this.myFrontlineRegions = myFrontlineRegions;
		this.state = state;
		this.armiesLeft = armiesLeft;
		this.visibleMaplst = visibleMaplst;
		this.placeArmiesMoves = new ArrayList<PlaceArmiesMove>();
		this.myName = state.getMyPlayerName();
		this.enemyName = state.getOpponentPlayerName();
	}
	public void run(){
		Collections.sort(myFrontlineRegions, new RegionArmyComparator());
		int armies = armiesLeft / myFrontlineRegions.size();
		if (armies == 0) {
			armies = 1;
			Iterator<Region> itRegion = myFrontlineRegions.iterator();
			Region region;
			while (armiesLeft > 0) {
				region = itRegion.next();
				region.addTempArmies(armies);
				armiesLeft -= armies;
				placeArmiesMoves
						.add(new PlaceArmiesMove(myName, region, armies));
			}

		} else {
			for (Region region : myFrontlineRegions) {
				region.addTempArmies(armies);
				placeArmiesMoves
						.add(new PlaceArmiesMove(myName, region, armies));
				armiesLeft -= armies;
			}
			armies = 1;
			Iterator<Region> itRegion = myFrontlineRegions.iterator();
			Region region;
			while (armiesLeft > 0) {
				region = itRegion.next();
				region.addTempArmies(armies);
				armiesLeft -= armies;
				placeArmiesMoves
						.add(new PlaceArmiesMove(myName, region, armies));
			}
		}
	}
	public int getArmiesLeft(){
		return armiesLeft;
	}
	public LinkedList<Region> getAttackEnemyRegions(){
		return visibleMaplst;
	}
	public ArrayList<PlaceArmiesMove> getPlaceArmiesMoves(){
		return placeArmiesMoves;
	}
}
