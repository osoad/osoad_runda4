package actions;

import java.util.ArrayList;
import java.util.LinkedList;

import map.Region;
import move.PlaceArmiesMove;
import bot.BotState;

public class PlaceArmies2RegionsConquer {
	ArrayList<Region> myFrontlineRegions;
	BotState state;
	int armiesLeft;
	LinkedList<Region> visibleMaplst;
	ArrayList<PlaceArmiesMove> placeArmiesMoves;
	String myName;
	String enemyName;
	public PlaceArmies2RegionsConquer(
			ArrayList<Region> myFrontlineRegions,
			BotState state,
			int armiesLeft,
			LinkedList<Region> visibleMaplst){
		this.myFrontlineRegions = myFrontlineRegions;
		this.state = state;
		this.armiesLeft = armiesLeft;
		this.visibleMaplst = visibleMaplst;
		this.placeArmiesMoves = new ArrayList<PlaceArmiesMove>();
		this.myName = state.getMyPlayerName();
		this.enemyName = state.getOpponentPlayerName();
	}
	public void run(){
		for(Region region : myFrontlineRegions){
			int armies = 0;
			for(Region enemyRegion : region.getNeighbors()){
				if(enemyRegion.ownedByPlayer(enemyName)){
					armies += enemyRegion.requiredArmies();
				}
			}
			int nrOfArmies = armies - region.getArmies() - region.getTempArmies() + 1; 
			if( nrOfArmies > 0 ){
				if(nrOfArmies < armiesLeft ){
					armiesLeft -= nrOfArmies; 
					region.addTempArmies(nrOfArmies);
					placeArmiesMoves.add( new PlaceArmiesMove(myName, region, nrOfArmies) );
				}
				else{
					region.addTempArmies(armiesLeft);
					placeArmiesMoves.add( new PlaceArmiesMove(myName, region, armiesLeft) );
					armiesLeft = 0;
					return;
				}
			}
		}
	}
	public int getArmiesLeft(){
		return armiesLeft;
	}
	public LinkedList<Region> getAttackEnemyRegions(){
		return visibleMaplst;
	}
	public ArrayList<PlaceArmiesMove> getPlaceArmiesMoves(){
		return placeArmiesMoves;
	}
}
