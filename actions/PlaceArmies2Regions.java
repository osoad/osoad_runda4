package actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import bot.BotState;

import map.Region;
import map.SuperRegion;
import move.PlaceArmiesMove;

import comparators.EnemyRegionComparator;

public class PlaceArmies2Regions {
	ArrayList<Region> myFrontlineRegions;
	BotState state;
	int armiesLeft;
	LinkedList<Region> visibleMaplst;
	ArrayList<PlaceArmiesMove> placeArmiesMoves;
	String myName;
	String enemyName;
	public PlaceArmies2Regions(
			ArrayList<Region> myFrontlineRegions,
			BotState state,
			int armiesLeft,
			LinkedList<Region> visibleMaplst){
		this.myFrontlineRegions = myFrontlineRegions;
		this.state = state;
		this.armiesLeft = armiesLeft;
		this.visibleMaplst = visibleMaplst;
		this.placeArmiesMoves = new ArrayList<PlaceArmiesMove>();
		this.myName = state.getMyPlayerName();
		this.enemyName = state.getOpponentPlayerName();
	}
	public void run(){
		for(Region region : myFrontlineRegions){
			int armies = 0;
			for(Region enemyRegion : region.getNeighbors()){
				if(enemyRegion.ownedByPlayer(enemyName)){
					armies += enemyRegion.getArmies() - 1;
				}
			}
			int nrOfArmies = armies - region.requiredArmies() + 1; 
			if( nrOfArmies > 0 ){
				if(nrOfArmies < armiesLeft ){
					armiesLeft -= nrOfArmies; 
					region.addTempArmies(nrOfArmies);
					placeArmiesMoves.add( new PlaceArmiesMove(myName, region, nrOfArmies) );
				}
				else{
					region.addTempArmies(armiesLeft);
					placeArmiesMoves.add( new PlaceArmiesMove(myName, region, armiesLeft) );
					armiesLeft = 0;
					return;
				}
			}
		}
	}
	public int getArmiesLeft(){
		return armiesLeft;
	}
	public LinkedList<Region> getAttackEnemyRegions(){
		return visibleMaplst;
	}
	public ArrayList<PlaceArmiesMove> getPlaceArmiesMoves(){
		return placeArmiesMoves;
	}
}
