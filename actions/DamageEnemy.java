package actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import comparators.FrontlineRegionsComparator;

import map.Region;
import map.SuperRegion;
import move.PlaceArmiesMove;
import bot.BotState;

public class DamageEnemy {
	BotState state;
	int armiesLeft;
	LinkedList<Region> visibleMaplst;
	ArrayList<PlaceArmiesMove> placeArmiesMoves;
	String myName;
	String enemyName;
	public DamageEnemy(
			BotState state,
			int armiesLeft,
			LinkedList<Region> visibleMaplst){
		this.state = state;
		this.armiesLeft = armiesLeft;
		this.visibleMaplst = visibleMaplst;
		this.placeArmiesMoves = new ArrayList<PlaceArmiesMove>();
		this.myName = state.getMyPlayerName();
		this.enemyName = state.getOpponentPlayerName();
	}
	public void run(){
		ArrayList<Region> myFrontlineRegions = getFrontlineRegions();
		Collections.sort(myFrontlineRegions, new FrontlineRegionsComparator(state.getMyPlayerName(),state.getOpponentPlayerName()));
		Collections.reverse(myFrontlineRegions);
		ArrayList<Region> importantRegions = new ArrayList<Region>();
		ArrayList<SuperRegion> used = new ArrayList<SuperRegion>();
		
		for(Region region : myFrontlineRegions){
			for(Region enemyRegion : region.getNeighbors()){
				if(enemyRegion.ownedByPlayer(enemyName)){
					if(!used.contains(enemyRegion.getSuperRegion()) &&
							enemyName.equals(enemyRegion.getSuperRegion().ownedByPlayer())){
						importantRegions.add(region);
						visibleMaplst.add(enemyRegion);
						used.add(enemyRegion.getSuperRegion());
					}
				}
			}
		}
		getPlaceArmies2RegionsConquer(importantRegions);
	}
	private void getPlaceArmies2RegionsConquer(ArrayList<Region> regions){
		PlaceArmies2RegionsConquer placeArmies2RegionsConquer =
				new PlaceArmies2RegionsConquer(regions,state,armiesLeft,visibleMaplst);
		placeArmies2RegionsConquer.run();
		armiesLeft = placeArmies2RegionsConquer.getArmiesLeft();
		visibleMaplst = placeArmies2RegionsConquer.getAttackEnemyRegions();
		placeArmiesMoves.addAll(placeArmies2RegionsConquer.getPlaceArmiesMoves());
		
	}
	private ArrayList<Region> getFrontlineRegions(){
		String myName = state.getMyPlayerName();
		String enemyName = state.getOpponentPlayerName();
		ArrayList<Region> myFrontlineRegions = new ArrayList<Region>();
		for (Region enemyRegion : state.getVisibleMap().getRegions()) {
			if (enemyRegion.ownedByPlayer(enemyName)) {
				if(!visibleMaplst.contains(enemyRegion)){
					visibleMaplst.add(enemyRegion);
				}
				
				for (Region enemyRegionNeighbor : enemyRegion.getNeighbors()) {
					if (enemyRegionNeighbor.ownedByPlayer(myName) ) {
						if (!myFrontlineRegions.contains(enemyRegionNeighbor)) {
							myFrontlineRegions.add(enemyRegionNeighbor);
						}
					}
				}
			}
		}
		return myFrontlineRegions;
	}
	public int getArmiesLeft(){
		return armiesLeft;
	}
	public LinkedList<Region> getAttackEnemyRegions(){
		return visibleMaplst;
	}
	public ArrayList<PlaceArmiesMove> getPlaceArmiesMoves(){
		return placeArmiesMoves;
	}
}
