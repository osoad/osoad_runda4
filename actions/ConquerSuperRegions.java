package actions;

import java.util.ArrayList;
import java.util.LinkedList;

import map.Region;
import map.SuperRegion;
import move.PlaceArmiesMove;

import bot.BotState;

public class ConquerSuperRegions {
	ArrayList<Integer> SuperRegions;
	LinkedList<Region> visibleMaplst;
	ArrayList<PlaceArmiesMove> placeArmiesMoves;
	BotState state;
	int armiesLeft;
	String myName;
	String enemyName;
	public ConquerSuperRegions(
			ArrayList<Integer> favouriteSuperRegions,
			BotState state,
			int armiesLeft,
			LinkedList<Region> visibleMaplst){
		this.SuperRegions = favouriteSuperRegions;
		this.state = state;
		this.armiesLeft = armiesLeft;
		this.visibleMaplst = visibleMaplst;
		this.placeArmiesMoves = new ArrayList<PlaceArmiesMove>();
		this.myName = state.getMyPlayerName();
		this.enemyName = state.getOpponentPlayerName();
		
	}
	public void run(){
		for(Integer superRegionId :SuperRegions){
			SuperRegion aux = state.getVisibleMap().getSuperRegion(superRegionId);
			if(aux != null){
				placeArmiesMoves.addAll(defendSuperRegion(aux));
				if(armiesLeft == 0 ){
					return;
				}
				if(!myName.equals(aux.ownedByPlayer())){
					placeArmiesMoves.addAll(conquerSuperRegion(aux));
				}
				if(armiesLeft == 0 ){
					return;
				}
			}
		}
	}
	public int getArmiesLeft(){
		return armiesLeft;
	}
	public LinkedList<Region> getAttackEnemyRegions(){
		return visibleMaplst;
	}
	public ArrayList<PlaceArmiesMove> getPlaceArmiesMoves(){
		return placeArmiesMoves;
	}
	
	private ArrayList<PlaceArmiesMove> defendSuperRegion(SuperRegion superRegion){
		System.err.println("Def " + superRegion.getId());
		ArrayList<PlaceArmiesMove> placeArmiesMoves = new ArrayList<PlaceArmiesMove>();
		DefendSuperRegion defendSuperRegion = new DefendSuperRegion(superRegion,armiesLeft,visibleMaplst,myName,enemyName);
		defendSuperRegion.run();
		armiesLeft = defendSuperRegion.getArmiesLeft();
		placeArmiesMoves.addAll(defendSuperRegion.getPlaceArmiesMoves());
		visibleMaplst = defendSuperRegion.getAttackEnemyRegions();
		return placeArmiesMoves;
	}
	private ArrayList<PlaceArmiesMove> conquerSuperRegion(SuperRegion superRegion){
		System.err.println("Con " + superRegion.getId());
		ArrayList<PlaceArmiesMove> placeArmiesMoves = new ArrayList<PlaceArmiesMove>();
		ConquerSuperRegion conquerSuperRegion = new ConquerSuperRegion(superRegion,armiesLeft,visibleMaplst,myName,enemyName);
		conquerSuperRegion.run();
		armiesLeft = conquerSuperRegion.getArmiesLeft();
		placeArmiesMoves.addAll(conquerSuperRegion.getPlaceArmiesMoves());
		visibleMaplst = conquerSuperRegion.getAttackEnemyRegions();
		return placeArmiesMoves;
	}
}
