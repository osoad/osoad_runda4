package utils;

import java.util.ArrayList;
import java.util.LinkedList;

import bot.BotState;

import map.Region;
import move.PlaceArmiesMove;

public class FrontlineRegions {
	BotState state;
	LinkedList<Region> visibleMaplst;
	String myName;
	String enemyName;
	public FrontlineRegions(
			BotState state,
			LinkedList<Region> visibleMaplst){
		this.state = state;
		this.visibleMaplst = visibleMaplst;
		this.myName = state.getMyPlayerName();
		this.enemyName = state.getOpponentPlayerName();
	}
	public ArrayList<Region> run(){
		ArrayList<Region> myFrontlineRegions = new ArrayList<Region>();
		for (Region enemyRegion : state.getVisibleMap().getRegions()) {
			if (!enemyRegion.ownedByPlayer(myName)) {
				if(!visibleMaplst.contains(enemyRegion)){
					visibleMaplst.add(enemyRegion);
				}
				for (Region enemyRegionNeighbor : enemyRegion.getNeighbors()) {
					if (enemyRegionNeighbor.ownedByPlayer(myName) ) {
						if (!myFrontlineRegions.contains(enemyRegionNeighbor)) {
							myFrontlineRegions.add(enemyRegionNeighbor);
						}
					}
				}
			}
		}
		return myFrontlineRegions;
	}
}
